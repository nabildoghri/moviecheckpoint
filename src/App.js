import logo from './logo.svg';
import './App.css';
import FiltredMovies from './FiltredMovies'
import MOVIESLIST from './movieList.json'
function App() {
  return (
    <div className="App">
        <FiltredMovies listeFilm={MOVIESLIST}></FiltredMovies>
    </div>
  );
}

export default App;
