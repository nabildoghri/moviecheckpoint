import React from 'react';
import { useState } from 'react';





function MovieList({listeFilm,clickedMovie}){
    var i =0
    return <div width="100%"  style={{ "padding":20,"margin":20,"border-radius": 25 ,"background": "lightGray"}}>
        <table width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Poster</th>
                    <th>Title</th>
                    <th>Rate</th>
                </tr>
            </thead>
            <tbody >
            {
            listeFilm.length?
                listeFilm.map(movie => {
                    return <tr key={"movie-"+i++}  id={i}  onClick={()=>clickedMovie(movie)} style={{backgroundColor:"gray"}}>
                    <td>{movie.id}</td>
                    <td><img src={movie.posterUrl} alt={movie.title} width="100px"></img></td>
                    <td>{movie.title}</td>
                    <td>{movie.rating}</td>
                </tr>
            }): <div>empty </div>}
            </tbody>
            
        </table>
    </div>
}
function MovieCard({movie}){
    return <div style={{ "padding":20,"margin":20,"border-radius": 25 ,"background": "Gray"}}>
        <div className="row" width="100%">
            <div className="col-8"><h1>{movie.title}</h1></div>
            <div className="col-4"><h3><b>Rating : {movie.rating}</b></h3></div>
        </div>
        <div className="row" width="100%">
            <div className="col"><img src={movie.posterUrl} alt={movie.title} width="100%"></img></div>
        </div>
        
        <div className="row" width="100%">
            <div className="col-12"><h3>{movie.plot}</h3></div>
            
        </div>
    </div>
}
function Filter({filterValue,valueChange,rateFilterValue, rateValueChange}){
    return <div className="row" style={{ "padding":20,"margin":20,"border-radius": 25 ,"background": "lightGray"}}>
    <div className="form-group col-6">
        <label for="titleFilter">Filtrer par Titre</label>
        <input type="text" className="form-control" id="titleFilter" value={filterValue}  placeholder="Enter title" onChange={(e)=>valueChange(e)}/>
         
        
    </div>
    <div className="col col-6">
    <label for="titleFilter">Filtrer par rating</label>
        <input type="number" className="form-control" id="rateFilter" value={rateFilterValue} onChange={(e)=>rateValueChange(e)} placeholder="Enter rate" />
    </div>
</div>
}

function FiltredMovies ({listeFilm}){
    const [movie, setMovieState] = useState(listeFilm[listeFilm.length -1])   
    const [filtredListFilm,setMovieList]= useState(listeFilm)
    const [filterValue,setFilterValue]= useState("")
    const [rateFilterValue,setRateFilterValue]=useState(0)
    // add movie state vlues 
    const [title,setTitle] = useState("")
    const [description,setDescription] = useState("")
    const [imgUrl,setUrl]=useState("")
    const [rating, setRating]=useState(0)

    const addMovie=(e)=>{
        var tmpMovie={
            "id": 2,
            "title": "The Cotton Club",
            "plot": "The Cotton Club was a famous night club in Harlem. The story follows the people that visited the club, those that ran it, and is peppered with the Jazz music that made it so famous.",
            "posterUrl": "https://images-na.ssl-images-amazon.com/images/M/MV5BMTU5ODAyNzA4OV5BMl5BanBnXkFtZTcwNzYwNTIzNA@@._V1_SX300.jpg",
            "rating":5
        }
        tmpMovie.id=listeFilm[listeFilm.length -1].id + 1 
        tmpMovie.title=title
        tmpMovie.plot= description
        tmpMovie.posterUrl=imgUrl
        tmpMovie.rating=rating
        listeFilm.push(tmpMovie)
        console.log(tmpMovie)
        setMovieList(listeFilm)
        setMovieState(tmpMovie)
    }
    ////////

    const handleClickedMovie=(movie)=>{
        console.log(movie)
        setMovieState(movie)
    }
    var tmpMovies=[]
    const handleRateFilterChange =(e)=>{
        listeFilm.forEach((movie)=>{
            if(movie.rating>=e.target.value){
                tmpMovies.push(movie)
            }
            setRateFilterValue(e.target.value)
            setMovieList(tmpMovies)
        });
    }
    const handleTitleFilterChange = (e)=>{
        listeFilm.forEach((movie)=>{
            if(movie.title.toUpperCase().indexOf(e.target.value.toUpperCase())>=0){
                console.log(tmpMovies)
                tmpMovies.push(movie)
            }
        })
        setFilterValue(e.target.value)
        setMovieList(tmpMovies)
    }

    return <div>
           <div style={{ "padding":20,"margin":20,"border-radius": 25 ,"background": "#73AD21"}}>
                <div className="row">
                    <div className="col-6">
                        <h1>Add new Movie</h1>
                    </div>
                    <div className="col-6">
                        <p>you can use this link to help you <a href="http://www.omdbapi.com/">omdbapi</a></p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                        <input type="text" className="form-control" id="title" value={title} onChange={(e)=>setTitle(e.target.value)} placeholder="Enter title" />
                        <input type="text" className="form-control" id="description" value={description} onChange={(e)=>setDescription(e.target.value)}   placeholder="Enter description" />
                    </div>
                    <div className="col-6">
                        <input type="text" className="form-control" id="poster" value={imgUrl}  onChange={(e)=>setUrl(e.target.value)} placeholder="Enter poster Url" />
                        <input type="number" className="form-control" id="rating"  value={rating} onChange={(e)=>setRating(e.target.value)} placeholder="Enter rating" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                        <button className="btn btn-info" onClick={(e)=>addMovie(e)}>Add movie</button>
                    </div>
                </div>
           </div>
            <Filter valueChange={handleTitleFilterChange} filterValue={filterValue} rateValueChange={handleRateFilterChange} rateFilterValue={rateFilterValue} />
            <div className="row">
                
                <div className="col-8">
                    <MovieList listeFilm={filtredListFilm} clickedMovie={handleClickedMovie} width="100%"/>
                </div>  
                <div className="col-4">
                    <MovieCard movie={movie}/> 
                </div>       
            </div>
        </div>
    
}

export default FiltredMovies
export {MovieList,Filter,MovieCard} 